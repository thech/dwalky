import React, { Component }                                     from 'react';
import MapView                                                  from './map/Map.js'
import './App.css';

class App extends Component {
    constructor(){
        super();
        this.state = {
            location: [],
            zoom: [0]
        }
    }
    render() {
        return (
            <MapView />
        );
    };
};

export default App;
