import React, {Component}                                       from "react";
import ReactMapboxGl, {Marker}                                  from "react-mapbox-gl";
import {parseString}                                            from "xml2js";
import {Map}                                                    from "immutable";
import config                                                   from "./config.json";
import './Map.css';

const {accessToken, style} = config;

function getCycleStations() {
    return fetch("https://tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml")
        .then(res => res.text())
        .then(data => {
            return new Promise((resolve, reject) => {
                parseString(data, (err, res) => {
                    if (!err) {
                        resolve(res.stations.station);
                    } else {
                        reject(err);
                    }
                });
            });
        })
}

function getLocation() {
    return navigator.geolocation.getCurrentPosition(function(position) {
        console.log("getting");
        return [position.coords.longitude, position.coords.latitude];
    })
}

const maxBounds = [
    [-0.481747846041145, 51.3233379650232], // South West
    [0.23441119994140536, 51.654967740310525], // North East
];

export default class MapView extends Component {

    constructor() {
        super();
        this.state = {
            center: [0, 0],
            zoom: [0],
            skip: 0,
            stations: new Map(),
            popupShowLabel: true
        }
    }

    componentWillMount() {
        this._setLocation();
    };

    componentDidMount() {
        //places request
    }

    _setLocation = () => {
        console.log("start");
        navigator.geolocation.getCurrentPosition((position) => {
            console.log([position.coords.longitude, position.coords.latitude]);
            this.setState({
                center: [position.coords.longitude, position.coords.latitude],
                zoom: [15]
            });
        }, (error) => {
                console.log(error)
            }, {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    };

    _markerClick = (station, {feature}) => {
        this.setState({
            center: feature.geometry.coordinates,
            zoom: [14],
            station
        });
    };

    _onDrag = () => {
        if (this.state.station) {
            this.setState({
                station: null
            });
        }
    };

    _onToggleHover(cursor, {map}) {
        map.getCanvas().style.cursor = cursor;
    }

    _onControlClick = (map, zoomDiff) => {
        const zoom = map.getZoom() + zoomDiff;
        this.setState({zoom: [zoom]});
    };

    _popupChange(popupShowLabel) {
        this.setState({popupShowLabel});
    }

    toggle = true;

    _onFitBoundsClick = () => {
        const {stations} = this.state;

        if (this.toggle) {
            this.setState({
                fitBounds: [[-0.122555629777, 51.4734862092], [-0.114842, 51.50621]]
            });
        } else {
            this.setState({
                fitBounds: [[32.958984, -5.353521], [43.50585, 5.615985]] // this won't focus on the area as there is a maxBounds
            });
        }

        this.toggle = !this.toggle;
    };

    render() {
        const {stations, station, skip, end, popupShowLabel, fitBounds} = this.state;

        return (
            <div className="container">
                <ReactMapboxGl
                    style={style}
                    fitBounds={fitBounds}
                    center={this.state.center}
                    zoom={this.state.zoom}
                    minZoom={0}
                    maxZoom={20}
                    accessToken={accessToken}
                    onDrag={this._onDrag}>
                    <Marker
                        coordinates={this.state.center}
                        anchor="bottom">
                        <img src="https://cdn1.iconfinder.com/data/icons/mirrored-twins-icon-set-hollow/512/PixelKit_point_marker_icon.png"/>
                    </Marker>
                </ReactMapboxGl>
                <div className={"btnWrapper " + (station ? "btnStationOpen" : "")}>
                    <button className="btn" onClick={this._onFitBoundsClick}>Fit to bounds</button>
                </div>
            </div>
        )
    }
}

